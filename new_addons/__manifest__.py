{
    "name"          : "New Addons",
    "version"       : "1.0",
    "author"        : "Miftahussalam",
    "website"       : "http://miftahussalam.com",
    "category"      : "New Module",
    "summary"       : "Ini adalah addons baru",
    "description"   : """
        Latihan membuat addons baru di odoo 10
    """,
    "depends"       : [
        "base",
    ],
    "data"          : [
        
    ],
    "demo"          : [],
    "test"          : [],
    "image"         : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}